#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo/*, DataBase &db*/)
{
	this->_players = players;
	//this->_db = db;
	/*
	try
	{
		DataBase::insertNewGame(this);
	}
	catch (const std::exception&)
	{
		***
	}
	*/
	
	//this->_questions = DataBase::initQuestions();

	for (vector<User*>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		(*it)->setCurrGame(this);
		this->_results.insert(pair<string,int>((*it)->getUsername(), 0));
	}
}

Game::~Game()
{
	for (std::vector<User*>::iterator it = this->_players.begin(); it != this->_players.end(); ++it)
	{
		delete (*it);
	}
	this->_players.clear();

	for (std::vector<Question*>::iterator it = this->_questions.begin(); it != this->_questions.end(); ++it)
	{
		delete (*it);
	}
	this->_questions.clear();
}

void Game::sendQuestionToAllUsers()
{
	string msg = "118";


	/*srand*/
	int qNum = rand() % this->_questions.size();

	msg += "###" + this->_questions[qNum]->getQuestion();

	for (int i = 0; i < 4; i++)
		msg += "###" + *(this->_questions[qNum]->getAnswers() + i);

	for (std::vector<User*>::iterator it = this->_players.begin(); it != this->_players.end(); ++it)
	{
		try
		{
			(*it)->send(msg);
		}
		catch (const std::exception&)
		{
			/***/
		}
	}
}
