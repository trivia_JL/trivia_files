#pragma once

#include "lib.h"

#define WIN32_LEAN_AND_MEAN

#pragma comment(lib, "Ws2_32.lib")


using namespace std;

class User
{
public:
	User(string username, SOCKET sock);
	void send(string message);

	string getUsername();
	SOCKET getSock();
	Room* getCurrRoom();
	Game* getCurrGame();

	void setCurrGame(Game* game);

	void clearRoom();
	bool createRoom(int id, string name, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* room);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	string _username;
	SOCKET _sock;
	Game *_currGame;
	Room *_currRoom;
};

